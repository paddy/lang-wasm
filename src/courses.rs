use crate::language::LanguageData;
use crate::{Anchor, Route};
use std::sync::Arc;
use yew::prelude::*;

pub struct Courses {
    props: CoursesProps,
}

#[derive(Properties, Clone)]
pub struct CoursesProps {
    pub data: Arc<LanguageData>,
}

impl Component for Courses {
    type Message = ();
    type Properties = CoursesProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {
            props,
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let language = &self.props.data.json;
        let courses: Vec<Html> = language.courses.iter().enumerate().map(|(i, c)| {
            let skills: Vec<Html> = c.skills.iter().map(|s| html! {
                <li>{&s.name}</li>
            }).collect();
            html! {
                <li>
                    <Anchor route=Route::Course(i) >
                        <tt>{&c.uuid}</tt>
                    </Anchor>
                    <ul>
                        {skills}
                    </ul>
                </li>
            }
        }).collect();

        html! {
            <>
                <h1>{"Kurse"}</h1>
                <ul>
                    {courses}
                </ul>
            </>
        }
    }
}
