use crate::{fl, Anchor, Route};
use yew::prelude::*;

pub struct Navbar {
    props: NavbarProperties,
}

#[derive(Properties, Clone)]
pub struct NavbarProperties {
    pub switch_lang: Callback<String>,
}

impl Component for Navbar {
    type Message = ();
    type Properties = NavbarProperties;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <nav id="menu">
                <ul>
                    <li>
                        <Anchor route=Route::Index>{"Kurse"}</Anchor>
                    </li>
                </ul>
                <ul>
                    <li><a href="#" onclick=self.props.switch_lang.reform(|_| "de".to_string())>{fl!("nav_german")}</a></li>
                    <li><a href="#" onclick=self.props.switch_lang.reform(|_| "en".to_string())>{fl!("nav_english")}</a></li>
                </ul>
            </nav>
        }
    }
}
