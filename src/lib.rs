#![recursion_limit = "256"]

use anyhow::Error;
use crate::language::{FetchResponse, LanguageData};
use crate::navbar::Navbar;
use crate::pages::index::Hello;
use crate::pages::course::Course;
use i18n_embed::{WebLanguageRequester, fluent::{
    FluentLanguageLoader, fluent_language_loader
}};
use lazy_static::lazy_static;
use rust_embed::RustEmbed;
use std::sync::Arc;
use std::sync::Mutex;
use unic_langid::LanguageIdentifier;
use wasm_bindgen::prelude::*;
use yew_router::prelude::*;
use yew::format::Json;
use yew::prelude::*;
use yew::services::fetch::FetchTask;

mod courses;
mod language;
mod navbar;
mod pages;

#[derive(RustEmbed)]
#[folder = "i18n"]
struct Localizations;

lazy_static! {
    static ref LANGUAGE_LOADER: FluentLanguageLoader = {
        fluent_language_loader!()
    };
}

#[macro_export]
macro_rules! fl {
    ($message_id:literal) => {{
        i18n_embed_fl::fl!($crate::LANGUAGE_LOADER, $message_id)
    }};

    ($message_id:literal, $($args:expr),*) => {{
        i18n_embed_fl::fl!($crate::LANGUAGE_LOADER, $message_id, $($args), *)
    }};
}

#[derive(Switch, Clone, PartialEq)]
enum Route {
    #[to = "/course/{id}"]
    Course(usize),
    #[to="/"]
    Index,
}

type Anchor = RouterAnchor<Route>;

pub struct State {
    data: Option<Arc<LanguageData>>,
}

pub enum Msg {
    LoadData,
    LoadDataSuccess(LanguageData),
    LoadDataError(Error),
    SwitchLang(String),
}

struct TestApp {
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
    error: Option<Error>,
    state: Arc<Mutex<State>>,
}

fn switch(state: Arc<Mutex<State>>, route: Route) -> Html {
    match route {
        Route::Course(id) => html! { <Course state=state id=id /> },
        Route::Index => html! { <Hello state=state /> },
    }
}

impl Component for TestApp {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let requested_languages = WebLanguageRequester::requested_languages();
        i18n_embed::select(
            &*LANGUAGE_LOADER,
            &Localizations,
            &requested_languages
        ).unwrap();

        link.send_message(Msg::LoadData);

        Self {
            link,
            task: None,
            error: None,
            state: Arc::new(Mutex::new(State {
                data: None,
            })),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::LoadData => {
                let handler = self.link
                    .callback(move |response: FetchResponse<LanguageData>| {
                        let (_, Json(data)) = response.into_parts();
                        match data {
                            Ok(data) => Msg::LoadDataSuccess(data),
                            Err(err) => Msg::LoadDataError(err),
                        }
                    });
                self.task = Some(language::load_data(handler));
                true
            },
            Msg::LoadDataSuccess(data) => {
                self.task = None;
                self.state.lock().unwrap().data = Some(Arc::new(data));
                true
            },
            Msg::LoadDataError(err) => {
                self.task = None;
                self.error = Some(err);
                true
            },
            Msg::SwitchLang(lang) => {
                let lang: LanguageIdentifier = lang.parse().unwrap();
                i18n_embed::select(&*LANGUAGE_LOADER, &Localizations, &[lang])
                    .unwrap();
                true
            },
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        if let Some(e) = &self.error {
            html! { <div>{e}</div> }
        } else if self.state.lock().unwrap().data.is_some() {
            let state = self.state.clone();
            let render = Router::render(
                move |route| switch(state.clone(), route)
            );
            let switch_lang =  self.link.callback(
                |lang: String| Msg::SwitchLang(lang)
            );
            html! {
                <>
                    <div>{fl!("loading")}</div>
                    <Navbar switch_lang=switch_lang />
                    <Router<Route, ()> render=render/>
                </>
            }
        } else {
            html! { <div>{fl!("loading")}</div> }
        }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    console_error_panic_hook::set_once();
    App::<TestApp>::new().mount_to_body();
}
