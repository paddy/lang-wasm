use crate::State;
use std::sync::{Arc, Mutex};
use yew::prelude::*;

pub struct Course {
    props: CourseProps,
}

#[derive(Properties, Clone)]
pub struct CourseProps {
    pub state: Arc<Mutex<State>>,
    pub id: usize,
}

impl Component for Course {
    type Message = ();
    type Properties = CourseProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let language_data = self.props.state.lock().unwrap().data.as_ref().unwrap().clone();
        let course = &language_data.json.courses[self.props.id];
        html! {
            <>
                <h1>{"Course"}</h1>
                <tt>{course.uuid}</tt>
            </>
        }
    }
}


