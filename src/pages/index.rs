use crate::courses::Courses;
use crate::State;
use std::sync::{Arc, Mutex};
use yew::prelude::*;

pub struct Hello {
    foo: bool,
    link: ComponentLink<Self>,
    props: IndexProps,
}

pub enum Msg {
    Toggle,
}

#[derive(Properties, Clone)]
pub struct IndexProps {
    pub state: Arc<Mutex<State>>,
}

impl Component for Hello {
    type Message = Msg;
    type Properties = IndexProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            props,
            foo: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Toggle => {
                self.foo = !self.foo;
                true
            },
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        let msg = if self.foo {
            html! { <span>{"Hallo Welt!"}</span> }
        } else {
            html! { <span>{"Halló heimur!"}</span> }
        };

        let language = self.props.state.lock().unwrap().data.as_ref().unwrap().clone();

        let words: Vec<Html> = language.json.words.iter().map(|w| html!{
            <li><tt>{&w.uuid}</tt> {"-"} {&w.lemma}</li>
        }).collect();

        let phrases: Vec<Html> = language.json.phrases.iter().map(|p| {
            let configs: Vec<Html> = p.configs.iter().map(|c| {
                let variants: Vec<Html> = c.0.iter().map(|v| html! {
                    html! {
                        <li>{v.get_text()}</li>
                    }
                }).collect();
                html! {
                    <ul>
                        {variants}
                    </ul>
                }
            }).collect();
            html! {
                <>
                    <hr />
                    {configs}
                </>
            }
        }).collect();

        html! {
            <>
                <div>
                    <Courses data=language />

                    <h1>{"Wörter"}</h1>
                    <ul>
                        {words}
                    </ul>

                    <h1>{"Phrasen"}</h1>
                    <ul>
                        {phrases}
                    </ul>
                </div>
                {msg}
                <button onclick=self.link.callback(move |_| Msg::Toggle)>{"Toggle"}</button>
            </>
        }
    }
}
