use anyhow::Error;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use uuid::Uuid;
use yew::callback::Callback;
use yew::format::{Json, Nothing};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Word {
    pub uuid: Uuid,
    pub language: String,
    pub lemma: String,
    pub forms: Vec<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all="kebab-case")]
#[serde(deny_unknown_fields)]
pub struct TranslationPart {
    pub text: String,
    pub word: Option<Uuid>,

    #[serde(default)]
    pub tags: Vec<String>,

    #[serde(default)]
    pub contract_with_previous: bool,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PhraseVariant {
    pub lang: String,
    pub text: String,
    pub parts: Option<Vec<TranslationPart>>,
    pub words: Vec<Uuid>,
    pub tags: Vec<String>,
}

impl PhraseVariant {
    pub fn get_text(&self) -> String {
        if let Some(parts) = &self.parts {
            parts.iter().map(|t| if t.contract_with_previous {
                t.text.clone()
            } else {
                " ".to_owned() + &t.text
            }).collect()
        } else {
            self.text.clone()
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PhraseConfig(pub Vec<PhraseVariant>);

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Phrase {
    pub uuid: Uuid,
    pub configs: Vec<PhraseConfig>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all="UPPERCASE")]
pub enum SkillType {
    Grammar,
    Words,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all="kebab-case")]
pub struct Skill {
    pub name: String,
    pub skill_type: SkillType,
    pub lesson: Option<Uuid>,
    pub emoji: Option<String>,

    #[serde(default)]
    pub words: Vec<Uuid>,

    #[serde(default)]
    pub tags: Vec<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Course {
    pub uuid: Uuid,
    pub skills: Vec<Skill>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageDataJson {
    pub words: Vec<Arc<Word>>,
    pub phrases: Vec<Arc<Phrase>>,
    pub courses: Vec<Arc<Course>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(from="LanguageDataJson")]
pub struct LanguageData {
    pub json: LanguageDataJson,
}

impl From<LanguageDataJson> for LanguageData {
    fn from(json: LanguageDataJson) -> Self {
        Self {
            json,
        }
    }
}

pub type FetchResponse<T> = Response<Json<Result<T, Error>>>;
type FetchCallback<T> = Callback<FetchResponse<T>>;

pub fn load_data(callback: FetchCallback<LanguageData>) -> FetchTask {
    let req = Request::get("/data.json")
        .body(Nothing)
        .unwrap();

    FetchService::fetch(req, callback).unwrap()
}
